import os

names = os.listdir()
try:
    names.pop(names.index(".git"))
    names.pop(names.index("reNamer.py"))
except ValueError:
    names.pop(names.index("reNamer.py"))
for i in range(0, len(names)):
    tempI = i
    extension = str(names[i]).split(".")[-1]
    try:
        os.rename(names[i], f'{i}temp.{extension}')
    except:
        print(names[i])
names = os.listdir()
try:
    names.pop(names.index(".git"))
    names.pop(names.index("reNamer.py"))
except ValueError:
    names.pop(names.index("reNamer.py"))
for i in range(0, len(names)):
    tempI = i
    try:
        fileExtension = names[i].split('.')[2]
        os.rename(names[i], f'{tempI+1}.{fileExtension}')
    except IndexError:
        fileExtension = names[i].split('.')[1]
        os.rename(names[i], f'{tempI+1}.{fileExtension}')
print('Renaming Completed')
